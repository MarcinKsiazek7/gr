'use strict';

const gulp = require('gulp');
const mjmlEngine = require('mjml').default;
const mjml = require('gulp-mjml');
const browsersync = require('browser-sync').create();
const panini = require('panini');
const imagemin = require("gulp-imagemin");
const del = require('del');

// Clean assets
function cleanDist() {
    return del(["./dist/*", "!./dist/*.zip"]);
}

function cleanMjml(done) {
    return del(["./src/mjml-dist"]);
    done();
}

// panini
function Panini(done) {
    gulp.src('src/mjml-src/*/*.mjml')
        .pipe(panini({
            root: 'src',
            layouts: 'src/layouts/',
            partials: 'src/partials/',
            helpers: 'src/helpers/',
            data: 'src/data/'
        }))
        .pipe(gulp.dest('src/mjml-dist'));
    done();
}

// Load updated HTML templates and partials into Panini
function resetPages(done) {
    panini.refresh();
    setTimeout(done, 500);
}

// Copy index.htmlo
function copyIndex(done) {
    gulp.src('./src/index.html')
        .pipe(gulp.dest('./dist/'));
    done();
}

// Optimize Images
function images() {
    return gulp
        .src("./src/img/**/*")
        // .pipe(newer("./_site/assets/img"))
        .pipe(
            imagemin([
                imagemin.gifsicle({
                    interlaced: true
                }),
                imagemin.jpegtran({
                    progressive: true
                }),
                imagemin.optipng({
                    optimizationLevel: 7
                }),
                imagemin.svgo({
                    plugins: [{
                        removeViewBox: false,
                        collapseGroups: true
                    }]
                })
            ], {
                verbose: true
            })
        )
        .pipe(gulp.dest("./dist/"));
}

// mjmlHtml
function mjmlHtml(done) {
    gulp.src('src/mjml-dist/*/*.mjml')
        .pipe(mjml(mjmlEngine, {
            minify: true
        }, {
            beautify: false
        }))
        .pipe(gulp.dest('./dist/'));
    done();
}

// mjmlHtmlBeautify
function mjmlHtmlBeautify(done) {
    gulp.src('src/mjml-dist/*/*.mjml')
        .pipe(mjml(mjmlEngine, {
            minify: false
        }, {
            beautify: true
        }))
        .pipe(gulp.dest('./dist'));
    done();
}

// BrowserSync
function browserSync(done) {
    browsersync.init({
        server: {
            baseDir: "./dist/"
        },
        port: 3000
    });
    done();
}

// BrowserSync Reload
function browserSyncReload(done) {
    browsersync.reload();
    done();
}

// Watch files
function watchFiles(done) {
    gulp.watch(['./src/mjml-src/*/*.mjml', './src/data/*.json', './partials/*.html'], links);
    gulp.watch('./src/img/*', images, browserSyncReload);
    done();
}

// define complex tasks
const links = gulp.series(cleanMjml, Panini, resetPages, mjmlHtml, browserSyncReload);
const buildBeautify = gulp.series(Panini, mjmlHtmlBeautify);
const build = gulp.series(Panini, mjmlHtml, copyIndex);
const watch = gulp.parallel(watchFiles, browserSync);
const dev = gulp.series(build, watch);


// export tasks
exports.clean = cleanDist;
exports.images = images;
exports.buildBeautify = buildBeautify;
exports.default = dev;